'''
========================================================================
This program is a part of personal programming project
Library No: 5
Library_name: PPP_KNN
------------------------------------------------------------------------
Machine learning algorithm: k-nearest neighbors(supervised machine learning algorithm for regrssion)
========================================================================
'''
# Import required libraries
#------------------------------------------------------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import timeit
#
# ======================================================================
# Preprocessing the dataset
# ======================================================================
#
class Data_preprocessing:
    '''
    ========================================================================
    Description:
    Data preprocessing is done to transform raw input data into an understandable and readable format.
    ======================================================================== 
    '''
    def import_dataset(self,data):
        '''
        ========================================================================
        Description:
        Reading in the input dataset and separating it into X:independent features and y:dependent feature.
        ------------------------------------------------------------------------
        Parameters:
        data: Input dataset with independent features and dependent feature
        ------------------------------------------------------------------------
        Conditions:
        The data file must be of the format .csv
        The data entries of the dataset should be real numbers and the empty onces should be filled based on the domain knowledge or zeros 
        There must exist atleast one independent feature and the dependent feature must be the last column of the dataset
        ------------------------------------------------------------------------
        Return:
        X: Dataframe of independent features; Size -> [No of samples x independent features]
        y: Dataframe of dependent feature; Size -> [No of samples]
        ======================================================================== 
        '''
        dataset = pd.read_csv(data)  # Reading in the input dataset
        X = dataset.iloc[:,:-1]      # Selecting all the columns of the input dataset except the last(which is the dependent feature/ target feature)
        y = dataset.iloc[:,-1]       # Selecting the last column of the input dataset
        return X,y

    def feature_scaling(self,X,y):
        '''
        ========================================================================
        Description:
        Standardizing the independent and dependent features into a unifrom scale.
        ------------------------------------------------------------------------
        Parameters:
        X: Dataframe of independent features; Size -> [No of samples x independent features]
        y: Dataframe of dependent feature; Size -> [No of samples]
        ------------------------------------------------------------------------
        Check:
        The mean of the standardized data = 0 (close enough)
        The standard deviation of the standardized data = 1 (close enough)
        ------------------------------------------------------------------------
        Return:
        scaled_X: Dataframe of uniformly scaled independent features; Size -> [No of samples x independent features]
        scaled_y: Dataframe of uniformly scaled dependent feature; Size -> [No of samples]
        mean_y: Mean value of the dependent feature; dtype -> float
        std_y: Standard deviation of the dependent feature; dtype -> float
        ------------------------------------------------------------------------
        Note:
        -This mean_y and std_y are later used to rescale the dependent feature back into its original scale
        ========================================================================
        '''
        mean_X, std_X = np.array([np.mean(X,axis =0)]), np.array([np.std(X,axis =0)]) # Mean computed row wise for independent features(@axis = 0)
        scaled_X = (X - mean_X)/std_X                                                 # Subtracting the original data from its mean value and dividing by its std.
        mean_y, std_y = np.array([np.mean(y)]), np.array([np.std(y)])
        scaled_y = (y - mean_y)/std_y
        return scaled_X, scaled_y, mean_y, std_y
    
    def split_dataset(self,X,y):
        '''
        ========================================================================
        Description:
        Randomly splitting the scaled independent and dependent features into training set and testing set, i.e. 80% of the samples are for training set 
        and remaining 20% is for the testing test. 
        ------------------------------------------------------------------------
        Parameters:
        scaled_X: Dataframe of uniformly scaled independent features; Size -> [No of samples x independent features]
        scaled_y: Dataframe of uniformly scaled dependent feature; Size -> [No of samples]
        ------------------------------------------------------------------------
        Return:
        X_train: Training set of independent features with 80% of the samples; Array size -> [80% of samples x independent features]
        X_test: Testing set of independent features with 20% of the samples; Array size -> [20% of samples x independent features]
        y_train: Training set of dependent feature with 80% of the samples; size -> [80% of samples]
        y_test: Testing set of dependent feature with 20% of the samples; size -> [20% of samples]
        ------------------------------------------------------------------------
        Example:
        scaled_X -> size = [437 x 25]
        After randomly splitting into training set and testing set,
        X_train.shape = (349,25)
        x_test.shape = (88,25)
        ========================================================================
        '''
        X_train, X_test = np.split(X.sample(frac = 1,random_state = 0).values,[int(0.8*len(X))]) # Splitting the scaled independent features randomly
        y_train, y_test = np.split(y.sample(frac = 1,random_state = 0).values,[int(0.8*len(y))]) # Splitting the scaled dependent feature randomly
        return X_train, X_test,y_train,y_test
#
# ======================================================================
# Construction of KNN algorithm
# ======================================================================
#
class Knearest_neighbor:
    '''
    ========================================================================
    Description: 
    Implementing KNN algorithm to find the nearest neighbors of each sample using their independent features.
    Comparing predicted results with the target feature with a prediction comparison plot.
    ======================================================================== 
    '''

    def predict_neighbor(self,X_train, X_test,y_train,y_test,k):
        '''
        ========================================================================
        Description:
        To find out dependent feature of each sample from its independent features with the help of a main dataset.
        ------------------------------------------------------------------------
        Parameters:
        X_train: Independent feature of the main dataset; Array size -> [80%  of samples x independent features]
        X_test: Independent feature of the test dataset; Array size -> [20% of samples x independent features]
        y_train: Dependent feature of the main dataset; size -> [80%  of samples]
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        k: No of samples to be considered as nearest neighbours; dtype -> int
        ------------------------------------------------------------------------
        Conditions:
        No of samples to be considered as nearest neighbours should be greater than zero
        ------------------------------------------------------------------------
        Return:
        y_pred: Predicted dependent feature from x_test independent features; size -> [20% of samples]     
        ------------------------------------------------------------------------
        Note:
        -The no of samples can be selected randomly or based on output and plot results: plot_KNeighbor.png, present in the directory: Plots_KNN_main
        -To get this output and plot execute PPP_KNN_main.py --kfind endlimit. endlimit is noting but arange(1,endlimit+1,1) from which we get the appropriate k; dtype -> int
        ========================================================================
        '''
        y_pred = np.zeros(y_test.shape)                               # To store prediction results                                     
        for row in range (len(X_test)):                               # Iterating through each sample of the independent features in the test dataset
            eucledian_distance = np.linalg.norm(X_train - X_test[row], axis = 1)  # Finding the norm of the independent features between the main and test dataset
            neighbor_index = eucledian_distance.argsort()[:k]         # Sorting the calculated distance list and selecting the indexes of the smallest norms based on k input
            nearest_neighbor = y_train[neighbor_index]                # Using the selected indexes we find the equivalent dependent feature from the main dataset
            y_pred[row] = nearest_neighbor.mean()                     # Now we calculate the prediction result of each sample by taking the mean of found dependent feature 
       
        return y_pred
    
    def prediction_comparison(self,y_test,y_pred,mean_y,std_y,Rsqr,RMSE,MSE):
        '''
        ========================================================================
        Description:
        To write result comparison between the predicted and the target result in a well tabulated manner.
        ------------------------------------------------------------------------
        Parameters:
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        y_pred: Predicted dependent feature from x_test independent features; size -> [20% of samples] 
        mean_y: Mean value of the dependent feature; dtype -> float
        std_y: Standard deviation of the dependent feature; dtype -> float
        Rsqr: Accuracy value; dtype -> float
        RMSE: RMSE loss value; dtype -> float
        MSE: MSE loss value; dtype -> float
        ------------------------------------------------------------------------
        Conditions:
        Make sure tabulate package is installed or pip install tabulate
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_TargetVSpred and writes a file: resultcomparision.txt 
        The file writes the predicted results and the target results
        ========================================================================
        '''
        # Rescaling computed prediction and target values
        #------------------------------------------------------------------------
        self.rescaled_y_test = (y_test*std_y + mean_y).reshape(-1,)     # Using the mean and std from scaling to rescale the target result as per dataset
        self.rescaled_y_pred = (y_pred*std_y + mean_y).reshape(-1,)     # Using the mean and std from scaling to rescale the predicted result as per dataset

        # To check the correctness of rescaling, accuracy is computed for the rescaled results and then compared with the original accuracy result
        #------------------------------------------------------------------------
        rescaled_Rsqr = model_accuracy.coefficient_of_determination(self.rescaled_y_test, self.rescaled_y_pred)

        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Results_TargetVSpred'):
                os.makedirs('Results_TargetVSpred')
        save_path = os.path.abspath('Results_TargetVSpred') # Save the file to the created directory

        # Creating a dataframe for comparing results so that the results can be written in a tablulated form
        #------------------------------------------------------------------------
        writedata = pd.DataFrame({'target':self.rescaled_y_test,'predicted':self.rescaled_y_pred})

        # writing the compared results and accuracy into the file -> resultcomparison.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path, 'resultcomparison.txt'), 'w') as f:

            print('Result comparison: Target vs Predicted (k-nearest neighbors)\n',file=f)  # Creating title infos before writing
            print(writedata.to_markdown(tablefmt='grid',floatfmt='.0f',index=False),file=f) # Tabulating the results in grid format without index
            print('\nComparing R^2 for correctness: ' + f'prediction: {Rsqr}, vs ' + f're-scaled prediction: {rescaled_Rsqr}',file=f) # checking for rescaling correctness
            print('\nFinal results of k-nearest neighbors:',file=f)
            print(f'R^2: {Rsqr}, ' + f'MSE_loss: {MSE}, '+ f'RMSE_loss: {RMSE} ',file=f)    # Results of the model KNN
            print('\nAbbreviations:',file=f)                                                # Adding required abbreviations
            print('R^2: Coefficient of determination(Accuracy)',file=f)
            print('MSE, RMSE: Mean squared error and Root mean squared error',file=f)
    
    def plot_prediction_result(self,Rsqr,RMSE,MSE):
        '''
        ========================================================================
        Description:
        Creating a plot for prediction results which includes predicted and the target result.
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exists: Plots_KNN_Main and returns a plot: plot_PredictionResults.png      
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_KNN_Main'):
                os.makedirs('Plots_KNN_Main')
        save_path = os.path.abspath('Plots_KNN_Main') # Save the file to the created directory

        # Plot for Prediction set results
        #------------------------------------------------------------------------
        plt.title('Testing result: Target vs Prediction')                                           # Set plot title
        plt.xlabel('No of samples')                                                                 # Set label for x-axis -> will be the no of sample
        plt.ylabel('Sample values')                                                                 # Set label for y-axis -> will be the values of each sample
        plt.plot(self.rescaled_y_test, 'o--',markersize = 7,color = 'red', label = 'Target data')   # Plotting target and prediction results
        plt.plot(self.rescaled_y_pred,'o-',markersize = 4,color='blue', label = 'Predicted data ($R^2$ = %.3f; MSE = %.4f; RMSE = %.4f)'%(Rsqr,MSE,RMSE))
        plt.legend(loc='lower left')
        fig = plt.gcf()                                                                             # Get the current figure
        fig.set_size_inches((9.5, 7),forward = False)                                               # Assigning plot size (width,height)
        fig.set_dpi(100)                                                                            # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path,'plot_PredictionResults.png'),bbox_inches='tight')       # Saving the plot
        plt.clf()                                                                                   # Cleaning the current figure after saving

    def plot_prediction_error(self):
        '''
        ========================================================================
        Description:
        Creating a error plot for the error between the target and the predicted results.
        Each plot depends on the loss input.
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exists: Plots_KNN_Main and returns a plot: plot_PredictionError.png     
        ------------------------------------------------------------------------
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_KNN_Main'):
                os.makedirs('Plots_KNN_Main')
        save_path = os.path.abspath('Plots_KNN_Main') # Save the file to the created directory
        
        # Plot for Testing set error
        #------------------------------------------------------------------------
        plt.title('Testing error')                                                           # Set plot title
        plt.xlabel('No of samples')                                                          # Set label for x-axis -> will be the no of sample
        plt.ylabel('Error value')                                                            # Set label for y-axis -> error value
        Error = (self.rescaled_y_test)-(self.rescaled_y_pred)                                # Compute error
        plt.plot(Error, 'o--',markersize = 7,color = 'red', label = 'Error')                 # Plotting testing set error
        plt.axhline(y=0, color = 'blue', linestyle = '--')                                   # Make horizontal line at zero error
        plt.legend()
        fig = plt.gcf()                                                                      # Get the current figure
        fig.set_size_inches((9.5 ,7),forward = False)                                        # Assigning plot size (width,height)
        fig.set_dpi(300)                                                                     # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path,'plot_PredictionError.png'),bbox_inches='tight')  # Saving the plot
        plt.clf()                                                                            # Cleaning the current figure after saving

#
# ======================================================================
# Construction of Determine_K to find the no of samples
# ======================================================================
#
class Determine_K:
    '''
    ========================================================================
    Description: 
    Determining the number of samples based on computed loss values.
    ======================================================================== 
    '''

    def select_K(self,X_train, X_test,y_train,y_test,loss,k):
        '''
        ========================================================================
        Description:
        Determining the no of samples required for efficient prediction based on kfind user input.  
        ------------------------------------------------------------------------
        Parameters:
        X_train: Independent feature of the main dataset; Array size -> [80%  of samples x independent features]
        X_test: Independent feature of the test dataset; Array size -> [20% of samples x independent features]
        y_train: Dependent feature of the main dataset; size -> [80%  of samples]
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        loss: Loss input; dtype -> str
        k: End limit to find the number of samples that gives efficient prediction results; dtype -> int
        ------------------------------------------------------------------------
        Conditions:
        The input of kfind i.e. the end limit should be greater than zero
        ------------------------------------------------------------------------
        Return:
        self.K[idx_min]: Selected no of samples; dtype -> int
        Creates a directory if one does not exists: Plots_KNN_Main and returns a plot: plot_KNeighbor.png      
        ========================================================================
        '''
        self.K = np.arange(1,k+1,1)                                                     # The range of samples to be tested for efficient prediction results
        self.timetaken_perK = []                                                        # Storage to note time taken for each sample
        RMSE_list = []                                                                  # To store RMSE loss values for each sample
        MSE_list = []                                                                   # To store MSE loss values for each sample
        for value in self.K:                                                            
            start_time = timeit.default_timer()                                         # Start timer to find the time taken to predict results for each sample
            y_pred= Kneighbor.predict_neighbor(X_train, X_test,y_train,y_test,value)    # Finding prediction results for each sample
            self.timetaken_perK.append(timeit.default_timer()-start_time)               # Stop timer. The time taken for each sample is noted; dtype -> float 

            MSE = model_loss.meanSquaredError_loss(y_test,y_pred)                       # Calculate MSE loss for each sample
            MSE_list.append(MSE)                                                        # Store calculated MSE loss
            RMSE = model_loss.rootmeanSquaredError_loss(y_test,y_pred)                  # Calculate RMSE loss for each sample
            RMSE_list.append(RMSE)                                                      # Store calculated RMSE loss
        idx_min = np.argmin(MSE_list) if loss == 'MSE' else np.argmin(RMSE_list)        # Note the index of the minimum loss from the loss list w.r.t the loss input

        # Plot to select no of samples
        #------------------------------------------------------------------------
        plt.title('Select K Neighbor')
        plt.xlabel('K Neighbor')
        if loss == 'MSE':
            plt.ylabel('MSE_loss')
            plt.plot(self.K,MSE_list,'o--',label='MSE')                                 # Select no of sample based on MSE loss
        elif loss == 'RMSE':
            plt.ylabel('RMSE_loss')
            plt.plot(self.K,RMSE_list,'o--',label='RMSE')                               # Select no of sample based on RMSE loss
        plt.legend()

        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_KNN_main'):
            os.makedirs('Plots_KNN_main') 
        save_path = os.path.abspath('Plots_KNN_main') # Save the file to the created directory
        
        fig = plt.gcf()                                                                 # Get the current figure
        fig.set_size_inches((7,5),forward = False)                                      # Assigning plot size (width,height)
        fig.set_dpi(100)                                                                # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path, 'plot_KNeighbor.png'),bbox_inches='tight')  # Saving the plot
        plt.clf()                                                                       # Cleaning the current figure after saving

        return self.K[idx_min]                                                          # Selected no of samples
        
    def write_time(self):
        '''
        ========================================================================
        Description:
        To create a comparison table that compares the time taken by each no of samples to make predictions.
        ------------------------------------------------------------------------
        Conditions:
        Make sure tabulate package is installed or pip install tabulate
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_Timetaken and and writes a file: resulttimetakenKNN.txt
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Results_Timetaken'):
            os.makedirs('Results_Timetaken')
        save_path = os.path.abspath('Results_Timetaken') # Save the file to the created directory

        # Creating a dataframe for time taken results so that the results can be written in a tablulated form
        #------------------------------------------------------------------------
        writedata = pd.DataFrame({'K': self.K,'Time':self.timetaken_perK})

        # writing time taken results into the file -> resulttimetakenKNN.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path, 'resulttimetakenKNN.txt'), 'w') as f:

            print('Time taken in seconds for prediction in KNN at different nearest neighbors(K) inputs:\n',file=f) # Creating title infos before writing
            print(writedata.to_markdown(tablefmt='grid',index=False),file=f)      # Tabulating the results in grid format without index
#
# ======================================================================
# Construction of Loss calculation
# ======================================================================
#
class LossCalculation:

    def meanSquaredError_loss(self,y_test,y_pred):
        '''
        ========================================================================
        Description:
        Computing loss per sample using mean squared error loss w.r.t dependent feature and predicted output.
        ------------------------------------------------------------------------
        Parameters:
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        y_pred: Predicted dependent feature from x_test independent features; size -> [20% of samples]]
        ------------------------------------------------------------------------
        Return:
        MSE: MSE loss value; dtype -> float
        ========================================================================
        '''
        MSE = np.mean((y_test - y_pred)**2)  
        return MSE

    def rootmeanSquaredError_loss(self,y_test,y_pred):
        '''
        ========================================================================
        Description:
        Computing loss per sample using root mean squared error loss w.r.t dependent feature and predicted output.
        ------------------------------------------------------------------------
        Parameters:
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        y_pred: Predicted dependent feature from x_test independent features; size -> [20% of samples]
        ------------------------------------------------------------------------
        Return:
        RMSE: RMSE loss value; dtype -> float
        ========================================================================
        '''
        RMSE = np.sqrt(np.mean((y_test - y_pred)**2))
        return RMSE
#
# ======================================================================
# Construction of Accuracy calculation
# ======================================================================
# 
class Accuracy:
    '''
    ========================================================================
    Description:
    Computing the accuracy of the model.
    ========================================================================
    ''' 
    def coefficient_of_determination(self,y_test,y_pred):
        '''
        ========================================================================
        Description:
        Computing the coefficient of determination w.r.t predicted output and dependent feature.
        ------------------------------------------------------------------------
        Parameters:
        y_test: Dependent feature of the main dataset; size -> [20% of samples]
        y_pred: Predicted dependent feature from x_test independent features; size -> [20% of samples]
        ------------------------------------------------------------------------
        Return:
        Rsqr: It returns the proportion of variation between the dependent feature and the predicted output of the model; dtype -> float
        ------------------------------------------------------------------------
        Note :
        -The best result is when the predicted values exactly match the dependent feature i.e. R^2 = 1 (close enough)
        ========================================================================
        '''
        SSres = np.sum((y_test - y_pred)**2)          # Computing the residual sum of squares (1)
        SStot = np.sum((y_test- np.mean(y_test))**2)  # Computing the total sum of squares (2)
        Rsqr = 1 - (SSres/SStot)                      # Computing Rsqr w.r.t (1) and (2)
        return Rsqr

# Initializing the class 
#------------------------------------------------------------------------
Kneighbor = Knearest_neighbor()          # K_Nearest Neighbor for regression
model_loss = LossCalculation()           # Computing model loss
model_accuracy = Accuracy()              # Computing model accuracy