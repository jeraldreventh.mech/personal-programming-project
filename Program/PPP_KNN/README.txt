DIRECTORY DETAILS
-----------------
Executable Files: 1
- PPP_KNN_main.py

Input files: 2
- PPP_KNN.py
- fatigue_dataset.csv(default dataset)/ fatigue_Selecteddataset.csv

The fallowing command will create all the results present in this directory
- python .\PPP_KNN_main.py

The results generated with fatigue_Selecteddataset.csv can be found in the directory 'Results_OtherDatasets'
Create results by executing - python .\PPP_KNN_main.py --data fatigue_Selecteddataset.csv --k 9
Refer Manual for how to generate results for different cases
----------------------------------------------------------------------------------------------------

# Details regarding the files and directories created can be referred from "Table 17" in the report.
