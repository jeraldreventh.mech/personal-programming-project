'''
========================================================================
This program is a part of personal programming project
file: PPP_KNN_main
------------------------------------------------------------------------
KNN algorithm is implemented to predict the dependent feature from the independent features
Prediction accuracy of KNN can be improved by finding the appropriate no of sample to select
Creates a comparison plot and a table that compares the predicted and the target result
Creates a comparison table that compares the time taken by each no of samples to make prediction 
========================================================================
'''
# Import created libraries
# -------------------------
from PPP_KNN import Data_preprocessing
from PPP_KNN import Knearest_neighbor, Determine_K
from PPP_KNN import Accuracy, LossCalculation

# Import required libraries
# -------------------------
import sys
import os
import click
import timeit

def check_inputs(k,loss,kfind):
    '''
    ========================================================================
    Description:
    To check whether certain user inputs are within the required limits.
    ------------------------------------------------------------------------
    Parameters:
    kfind: Find the no of sample(k) within a range of samples with lower loss value; dtype -> int
    k: The selected no of sample(k) based on kfind; dtype -> int
    loss: Loss input; dtype -> str
    ------------------------------------------------------------------------
    Note:
    -If the inputs are not within the options range, program exits by throwing an error with possible inputs
    ========================================================================
    '''
    # Checking whether the input is correct or wrong
    #------------------------------------------------------------------------
    inputset, optionset = [k,kfind], ['k','kfind']                       # Grouping similar inputs and their options togather
   
    for idx,input in enumerate(inputset):                                  
        if input <= 0:                                                   # Checking for correctness
            sys.exit('Error: '+str(optionset[idx])+' input must be > 0') # If the input condition is not satisfied the program exits by throwing an error                                                      
    if (not loss == 'MSE') and (not loss == 'RMSE'):            # If the inputs are not within the options range program exits by throwing an error mentioning possible inputs
        sys.exit('Error: Recheck loss input\nPossible inputs: MSE or RMSE') 
#   
# ======================================================================
# User selection of Adjustable inputs -> Implementing click
# ======================================================================
#
@click.command()
@click.option('--data',nargs=1,type=str,default='fatigue_dataset.csv',help='Enter input dataset.csv: last column must be the target feature')
@click.option('--loss',nargs=1,type=str,default='MSE',help='Select loss for kfind : [MSE or RMSE]')
@click.option('--kfind',nargs=1,type=int,default=9,help='Enter end limit(>0) to find nearest neighbor(k)')
@click.option('--k',nargs=1,type=int,default=3,help='Select nearest neighbor(k) from the output or plot_KNeighbor.png')
#
# ============================================================================================================================================
#                                                      CREATING MODEL --> KNN REGRESSION
# ============================================================================================================================================
#
def KNN_regression(data,loss,kfind,k):
    '''
    ========================================================================
    Description:
    This KNN REGRESSION model makes accurate prediction results based on selected no of samples. Initially assign a random value for no of samples(k) and endlimit shall
    be within a range of ten. After executing the model one can determine the selected no of sample from the output of the code or from the plot: plot_KNeighbor.png.
    ========================================================================
    '''
    # Check certain user inputs
    #------------------------------------------------------------------------
    check_inputs(k,loss,kfind)

    # Initialization for KNN
    #------------------------------------------------------------------------
    Preprocessed_data = Data_preprocessing()            # Preprocessing the dataset
    Kneighbor = Knearest_neighbor()                     # K_Nearest Neighbor for regression
    model_accuracy = Accuracy()                         # Computing model accuracy
    model_loss = LossCalculation()                      # Computing model loss
    K = Determine_K()                                   # Finding suitable K for higher accuracy 

    # Data_preprocessing
    #------------------------------------------------------------------------
    start_time = timeit.default_timer()                 # Start timer to note the time taken by the model         
    X,y = Preprocessed_data.import_dataset(data)        # X: Independent feature, y: dependent feature           
    scaled_X,scaled_y,mean_y,std_y = Preprocessed_data.feature_scaling(X,y)              # Scaled features, mean and std for rescaling during comparison
    X_train, X_test,y_train,y_test = Preprocessed_data.split_dataset(scaled_X,scaled_y)  # Splitting dataset into training and test set

    # Knearest_neighbor
    #------------------------------------------------------------------------
    y_pred= Kneighbor.predict_neighbor(X_train, X_test,y_train,y_test,k)            # Calculate prediction results

    # Accuracy calculation
    #------------------------------------------------------------------------
    Rsqr = model_accuracy.coefficient_of_determination(y_test,y_pred)               # Compute accuracy value

    # Loss calculation
    #------------------------------------------------------------------------
    MSE = model_loss.meanSquaredError_loss(y_test,y_pred)                           # Compute loss value @MSE
    RMSE = model_loss.rootmeanSquaredError_loss(y_test,y_pred)                      # Compute loss value @RMSE

    # Find K with lower prediction loss
    # kfind: is the end limit of the list of k values starting from 1 to kfind from which appropriate k is identified for accurate prediction
    #------------------------------------------------------------------------
    
    # Plot to determine_K
    #------------------------------------------------------------------------
    bestk = K.select_K(X_train, X_test,y_train,y_test,loss,kfind)                   # Return the result of kfind
    print('\nResults of kfind:\nNo of nearest neighbors(k) to be selected to get efficient prediction results = %d\n'% bestk)

    # Write time taken
    #------------------------------------------------------------------------
    K.write_time()
     
    # Write results
    #------------------------------------------------------------------------
    Kneighbor.prediction_comparison(y_test,y_pred,mean_y,std_y,Rsqr,RMSE,MSE)       # Write prediction comparison

    # Plot prediction and error
    #------------------------------------------------------------------------
    Kneighbor.plot_prediction_result(Rsqr,RMSE,MSE)                                 # Make prediction plot
    Kneighbor.plot_prediction_error()                                               # Make error plot
    timetaken_permod=timeit.default_timer()-start_time                              # Stop timer. Time taken by the model to execute is noted

    # Write time taken by the model
    #------------------------------------------------------------------------
    save_path = os.path.abspath('Results_Timetaken') # Save the file to the created directory
    with open(os.path.join(save_path, 'resulttimetakenKNN.txt'), 'a') as f:
        print('\nTime taken by the model to execute:',timetaken_permod,file=f)      # Write time taken by KNN to execute

if __name__ == '__main__':
    KNN_regression()
