'''
========================================================================
This program is a part of personal programming project  
file: PPP_Overallresults
------------------------------------------------------------------------
Creates a comparison Excel with the target features and predicted results of all the three Machine learning regression models 
Creates a performance analysis plot w.r.t the three Machine learning regression models for predicting the target feature
Condition: This program should be executed only after executing all the programs of the three Machine learning regression models
Note: All these predicted results are calculated based on the best performing combinations of loss and optimizer (ANN & MLR)
========================================================================
'''
# Import required libraries
# -------------------------
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import click
#
# ======================================================================
# Creating plots and datas for all the three combinations of implemented Machine learning regression models 
# ======================================================================
#
class Result_comparison:
    '''
    ========================================================================
    Description: 
    Reading in the prediction results, accuracy and loss value from the best performing combinations of the three ML models.
    Reading in target feature and using this read-in results to create comaprision Excel and a overall summary plot.
    ========================================================================
    '''

    def __init__(self,models,loss,lossmlr,lossknn,optimizer,y_test):
        '''
        ========================================================================
        Description:
        Initializing the inputs and creating storage to store data.  
        ------------------------------------------------------------------------
        Parameters:
        models: List of best performing models from ANN,MLR & KNN; dtype -> str
        loss: Loss input from best performing combination in ANN; dtype -> str
        lossmlr: Loss input from best performing combination in MLR; dtype -> str
        lossknn: Loss input from KNN; dtype -> str
        optimizer: Optimizer input from best performing combination in ANN; dtype -> str
        y_test: No of samples in the test case; dtype -> int
        ------------------------------------------------------------------------
        Note:
        No of samples in the test case is equivalent to the no of samples in the predicted results
        ========================================================================
        '''
        self.models = models
        self.loss,self.lossMLR,self.lossKNN = loss,lossmlr,lossknn
        self.optimizer = optimizer
        self.len_ytest = y_test

        # Creating Storage to store read in data of different combination to make plots and data comparison
        #------------------------------------------------------------------------
        self.pred_loss,self.pred_Acc = [],[]                                    # To store prediction loss and accuracy
        self.target,self.pred_ANN = [],[]                                       # To store target feature and prediction results from ANN
        self.pred_MLR,self.pred_KNN = [],[]                                     # To store prediction results from MLR and KNN 

    def read_data(self):
        '''
        ========================================================================
        Description:
        Reading in data to make comparison Excel and plot summary of the best performing combinations of the three ML models.
        ========================================================================
        '''
        # Read target feature from the file: resultcomparison_lossinput_optimizerinput.txt in the directory: PPP_ANN/Results_TargetVSpred
        #------------------------------------------------------------------------ 
        open_path = os.path.abspath('PPP_ANN/Results_TargetVSpred')
        filename = os.path.join(open_path,'resultcomparison_%s_%s.txt'%(self.loss,self.optimizer)) 
        with open(filename, 'r') as f:
            read_data = f.readlines()
            for idx in (np.arange(7,2*self.len_ytest+7,2)):
                self.target.append(float(read_data[idx].split('|')[1]))                             # Storing target features
        
        # Read prediction results of the ANN best performing combinations from the same file
        #------------------------------------------------------------------------
        with open(filename, 'r') as f:
            read_data = f.readlines()
            for idx in (np.arange(7,2*self.len_ytest+7,2)):
                self.pred_ANN.append(float(read_data[idx].split('|')[2]))                           # Storing prediction results from ANN best performing combinations
        
        # Read loss and accuracy of the ANN best performing combinations from the file: testingresults_lossinput_optimizerinput.txt in the directory: PPP_ANN/Results_Testing
        #------------------------------------------------------------------------
        open_path = os.path.abspath('PPP_ANN/Results_Testing')
        filename = os.path.join(open_path,'testingresults_%s_%s.txt'%(self.loss,self.optimizer))
        with open(filename, 'r') as f:
            read_data = f.readlines()
            data = read_data[4].split(':')
            self.pred_loss.append(float(data[3].split('\n')[0]))                                    # Storing loss value from ANN best performing combinations
            self.pred_Acc.append(float(data[2].split(',')[0]))                                      # Storing accuracy value from ANN best performing combinations

        # Read prediction results of the MLR best performing combinations from the file: resultcomparison_lossinput.txt in the directory: PPP_MLR/Results_TargetVSpred
        #------------------------------------------------------------------------
        open_path = os.path.abspath('PPP_MLR/Results_TargetVSpred')
        filename = os.path.join(open_path,'resultcomparison_%s.txt'%(self.lossMLR))
        with open(filename, 'r') as f:
            read_data = f.readlines()
            for idx in (np.arange(7,2*self.len_ytest+7,2)):
                self.pred_MLR.append(float(read_data[idx].split('|')[2]))                           # Storing prediction results from MLR best performing loss combination
        
        # Read loss and accuracy of the MLR best performing combinations from the file: testingresults_lossinput.txt in the directory: PPP_MLR/Results_Testing
        #------------------------------------------------------------------------
        open_path = os.path.abspath('PPP_MLR/Results_Testing')
        filename = os.path.join(open_path,'testingresults_%s.txt'%(self.lossMLR))
        with open(filename, 'r') as f:
            read_data = f.readlines()
            data = read_data[4].split(':')
            self.pred_loss.append(float(data[3].split('\n')[0]))                                    # Storing loss value from MLR best performing loss combination
            self.pred_Acc.append(float(data[2].split(',')[0]))                                      # Storing accuracy value from MLR best performing loss combination

        # Read predicted results of KNN from the file: resultcomparison.txt in the directory: PPP_KNN/Results_TargetVSpred
        #------------------------------------------------------------------------
        open_path = os.path.abspath('PPP_KNN/Results_TargetVSpred')
        filename = os.path.join(open_path,'resultcomparison.txt')
        with open(filename, 'r') as f:
            read_data = f.readlines()
            for idx in (np.arange(5,2*self.len_ytest+5,2)):
                self.pred_KNN.append(float(read_data[idx].split('|')[2]))                           # Storing prediction results of KNN
       
        # Read loss and accuracy of KNN from the same file
        with open(filename, 'r') as f:
            read_data = f.readlines()
            read = 2 if self.lossKNN == 'MSE' else 3
            data = read_data[2*self.len_ytest+9].split(':')
            self.pred_loss.append(float(data[read].split(',')[0]))                                  # Storing loss value of KNN
            self.pred_Acc.append(float(data[1].split(',')[0]))                                      # Storing accuracy value of KNN
    
    def write_TargetVSpred(self):
        '''
        ========================================================================
        Description:
        To create a comparison Excel between the target results and the predicted results of the best performing combinations of the three ML models. 
        From this Excel we can compare the predicted results of the combinations for each sample(line item) against the target results and perform result analysis.
        ------------------------------------------------------------------------
        Conditions:
        make sure xlsxwriter is available or pip install xlsxwriter
        ------------------------------------------------------------------------
        Output:
        Writes a file: resultcomparisonOverall.xlsx to the directory: PPP_Overallresults         
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('PPP_Overallresults'):
            os.makedirs('PPP_Overallresults') 
        save_path = os.path.abspath('PPP_Overallresults')   # Save the file to the created directory

        # Creating inputs for each column of the dataframe from the stored target feature and prediction results
        #------------------------------------------------------------------------
        c1,c2,c3,c4 = self.target,self.pred_ANN,self.pred_MLR,self.pred_KNN

        # Creating a dataframe to write results in a tablulated xlsx format
        #-----------------------------------------------------------------------
        writedata = pd.DataFrame({'Target':c1,'Pred_ANN_%s'%(self.optimizer):c2, 'Pred_MLR':c3, 'Pred_KNN':c4})

        # Writing an Excel
        #------------------------------------------------------------------------
        writer = pd.ExcelWriter(os.path.join(save_path, 'resultcomparisonOverall.xlsx'))                        # Creating an Excel writer
        writedata.to_excel(writer, sheet_name='targetVSpred',float_format = '%.0f',index = False,startrow=1)    # Converting dataframe into an Excel by specifying the sheet name, 
                                                                                                                # floating format, starting row and avoiding dataframe index
        # Getting xlsxwriter objects from the dataframe writer object
        #------------------------------------------------------------------------                                                                                                        
        getworkbook = writer.book
        getworksheet = writer.sheets['targetVSpred']                                                            # Specify the sheet name

        writetitle = 'Result comparison: Target vs Predicted for used ML models'                                # Setting title for the Excel
        getworksheet.write(0,0,writetitle)                                                                      # Writing the title in the Excel at a specified location
        cellformat_title = getworkbook.add_format({'bold':True,'font_size': 14})                                # Formating the title by making it bold and setting a font size 
        getworksheet.set_row(0,40,cellformat_title)                                                             # Applying the formating and setting the title row height

        cellformat_border = getworkbook.add_format({'border':1})                                                # Setting closed borders to the Excel
        getworksheet.conditional_format('A1:D%d'%(self.len_ytest+2), {'type':'no_blanks','format':cellformat_border}) # Applying boarder settings for required rows and cols
        cellformat_column = getworkbook.add_format({'align':'center'})                                          # Setting center alignment to the datas
        getworksheet.set_column('A1:D%d'%(self.len_ytest+2), 16,cellformat_column)                              # Applying the alignment setting and adjusting the column length
        
        # Using dataframe to calculate the mean and the std of each combination and writing it in the Excel
        #------------------------------------------------------------------------
        mean,std = np.array(writedata.mean()),np.array(writedata.std())                                         # Calculating mean and std
        writemean = pd.DataFrame({'Target':[mean[0]],'Pred_ANN_%s'%(self.optimizer):[mean[1]], 'Pred_MLR':[mean[2]], 'Pred_KNN':[mean[3]]})
        writestd = pd.DataFrame({'Target':[std[0]],'Pred_ANN_%s'%(self.optimizer):[std[1]], 'Pred_MLR':[std[2]], 'Pred_KNN':[std[3]]})
        writemean.to_excel(writer, sheet_name='targetVSpred',float_format = '%.3f',header = False,index = False,startrow=self.len_ytest+3) # Converting dataframe into an Excel
        writestd.to_excel(writer, sheet_name='targetVSpred',float_format = '%.3f',header = False,index = False,startrow=self.len_ytest+4)  # Converting dataframe into an Excel

        bold = getworkbook.add_format({'bold': 1})                                                              # Setting bold formating
        getworksheet.write(self.len_ytest+3,4,'Mean',bold)                                                      # Writing the mean in the Excel at a specified location
        getworksheet.write(self.len_ytest+4,4,'Std',bold)                                                       # Writing the std in the Excel at a specified location
        writer.save()

    def write_Modelperformance(self):
        '''
        ========================================================================
        Description:
        To write loss and accuracy values of all the best performing combinations of ANN, MLR and KNN.
        ------------------------------------------------------------------------
        Conditions:
        Make sure tabulate package is installed or pip install tabulate
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exists: PPP_Overallresults and writes a file: resultModelperformance.txt  
        ========================================================================
        '''
        self.MSE_loss = []                                                           # Creating storage to store MSE loss values for all the best performing combinations
        self.RMSE_loss = []                                                          # Creating storage to store RMSE loss values for all the best performing combinations

        # Read loss and accuracy of all the best performing combinations of ANN and MLR
        #------------------------------------------------------------------------
        open_path1 = os.path.abspath('PPP_ANN/Results_Testing')                            # Assigning paths once again to access the directories
        open_path2 = os.path.abspath('PPP_MLR/Results_Testing')
        open_path3 = os.path.abspath('PPP_KNN/Results_TargetVSpred')
        filename1 = os.path.join(open_path3,'resultcomparison.txt')

        for loss in ['MSE','RMSE']:
            filename2 = os.path.join(open_path1,'testingresults_%s_%s.txt'%(loss,self.optimizer))
            filename3 = os.path.join(open_path2,'testingresults_%s.txt'%(loss))
            for filename in [filename2,filename3]:
                with open(filename, 'r') as f:
                    read_data = f.readlines()
                    data = read_data[4].split(':')
                    if loss == 'MSE':                                                      # Storing loss value from all the best performing combinations of ANN & MLR
                        self.MSE_loss.append(float(data[3].split('\n')[0]))
                    else:
                        self.RMSE_loss.append(float(data[3].split('\n')[0]))             

        # Read loss and accuracy of KNN from the same file
        #------------------------------------------------------------------------
        with open(filename1, 'r') as f:
            read_data = f.readlines()
            data = read_data[2*self.len_ytest+9].split(':') 
            self.MSE_loss.append(float(data[2].split(',')[0]))                            # Storing MSE loss value of KNN
            self.RMSE_loss.append(float(data[3].split(',')[0]))                           # Storing RMSE loss value of KNN

        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('PPP_Overallresults'):
            os.makedirs('PPP_Overallresults') 
        save_path = os.path.abspath('PPP_Overallresults')   # Save the file to the created directory

        # Creating a dataframe for the results to be ploted so that the results can be written in a tablulated form
        #------------------------------------------------------------------------
        method = ['ANN','MLR','KNN'] # Method list to tabulate
        writedata = pd.DataFrame({'Method': method,'R^2':self.pred_Acc,'MSE':self.MSE_loss,'RMSE': self.RMSE_loss})

        # writing loss and accuracy results into the file -> resultModelperformance.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path, 'resultModelperformance.txt'), 'w') as f:

            print('Performance analysis of all the best performing combinations of ANN, MLR and KNN :\n',file=f)  # Creating title infos before writing 
            print(writedata.to_markdown(tablefmt='grid',floatfmt='.6f',index=False),file=f)                       # Tabulating the results in grid format without index
            print('\nAbbreviations:',file=f)                                                                      # Adding required abbreviations
            print('R^2: Coefficient of determination(Accuracy)\nMSE: Mean squared error loss\nRMSE: Root mean squared loss',file=f)

    def plot_OverallResult(self,targetFeature,Acclim,lslim):
        '''
        ========================================================================
        Description:
        Creating an analysis plot comparing the performance of the best performing combinations of the three ML models.
        In the next grid prediction results of best performing model will be plotted based on user inputs of loss and optimizer.
        ------------------------------------------------------------------------
        Parameters:
        targetFeature: Input dependent feature title; dtype -> str
        Acclim: y-limit input for accuracy; dtype -> float
        lslim: y-limit input for loss; dtype -> float
        ------------------------------------------------------------------------
        Output:
        Creates directory if one does not exist: PPP_Overallresults and saves the plot: plotOverall_results.png
        ========================================================================
        '''
        fig = plt.figure(figsize=(11,5))                                                        # Setting plot size
        axes = plt.subplot(121)                                                                 # Setting subplots[1 row 2 columns]
        twin_axis = axes.twinx()                                                                # Creating twin axis for the loss

        axes.set_title('ML Models for %s'%(targetFeature))                                      # Set plot title
        axes.set_xlabel('Models')                                                               # Set label for x-axis -> will be the models
        axes.set_ylabel('$R^2$ value')                                                          # Set label for y-axis -> will be the prediction accuracy
        twin_axis.set_ylabel('Loss value')                                                      # Set label for twin y-axis -> will be the prediction loss
        axes.set_ylim(Acclim[0],Acclim[1])                                                      # Set y-lim for accuracy based on user inputs
        twin_axis.set_ylim(lslim[0],lslim[1])                                                   # Set y-lim for loss based on user inputs
        
        axes.bar(self.models, self.pred_Acc,edgecolor='C0',fill=False,width=0.4,label='$R^2$')  # Making bar chart of the calculated accuracy w.r.t the models
        twin_axis.plot(self.pred_loss,'o--',color='C1',label='loss')                    # Making plot of the calculated loss in the twin axis w.r.t the models

        plt1, label1 = axes.get_legend_handles_labels()                                         # Creating labels for axis and twin axis
        plt2, label2 = twin_axis.get_legend_handles_labels()
        twin_axis.legend(plt1 + plt2, label1 + label2, loc=0,ncol=2)

        # Making plot for the testing results of best performing combination
        axes1 = plt.subplot(122)
        loss = 'MSE' if self.loss == 'MSE' else 'RMSE'                                          # Loss based on user input loss from ANN
        
        label = '($R^2$=%.3f; %s=%.4f)'%(self.pred_Acc[0],loss,self.pred_loss[0])               # Creating labels
        axes1.set_title('Target vs Prediction (@ANN_%s)'%(self.optimizer))                      # Set plot title
        axes1.set_xlabel('No of samples')                                                       # Set label for x-axis -> will be the no of sample
        axes1.set_ylabel('Sample values')                                                       # Set label for y-axis -> will be the values of each sample
        axes1.plot(self.target, 'o--',markersize = 7,color = 'red', label = str(targetFeature))
        axes1.plot(self.pred_ANN,'o-',markersize = 4,color = 'blue',label = 'Predicted'+str(label))
        axes1.legend()
        fig.tight_layout()                                                                      # Set tight layout

        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('PPP_Overallresults'):
            os.makedirs('PPP_Overallresults') 
        save_path = os.path.abspath('PPP_Overallresults') # Save the file to the created directory

        fig = plt.gcf()                                                                         # Get the current figure
        fig.set_size_inches((12,5.7),forward = False)                                           # Assigning plot size (width,height)
        fig.set_dpi(300)                                                                        # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path, 'plotOverall_results.png'),bbox_inches='tight')     # Saving the plot
        plt.clf()                                                                               # Cleaning the current figure after saving
#
# ======================================================================
# User selection of Adjustable inputs -> Implementing click
# ======================================================================
#
@click.command()
@click.option('--models',nargs=3,type=str,default=(['ANN_Adam','MLR','KNN']),help='Enter model input(Ex: ANN_Optimizer)')
@click.option('--loss',nargs=1,type=str,default='MSE',help='Select loss: [MSE or RMSE] for ANN')
@click.option('--lossmlr',nargs=1,type=str,default='MSE',help='Select loss: [MSE or RMSE] for MLR')
@click.option('--lossknn',nargs=1,type=str,default='MSE',help='Select loss: [MSE or RMSE] for KNN')
@click.option('--optimizer',nargs=1,type=str,default='Adam',help='Select optimizer: [SGD,SGDM,RMSP or Adam] for ANN')
@click.option('--len_test',nargs=1,type=int,default=88,help='Enter the no of samples in test case(i.e. 20 percent of the total sample)')
@click.option('--targetf',nargs=1,type=str,default='Fatigue Strength',help='Enter the name of the target feature')
@click.option('--limacc',nargs=2,type=float,default=([0.93,0.99]),help='Enter lw & up bound for accuracy values')
@click.option('--limls',nargs=2,type=float,default=([0.02,0.08]),help='Enter lw & up bound for loss values')  #@ RMSE [0.113,0.268]
#
# ============================================================================================================================================
#                                                      IMPLEMENTED MACHINE LEARNING MODELS ANALYSIS
# ============================================================================================================================================
#
def Overallresults(models,loss,lossmlr,lossknn,optimizer,len_test,targetf,limacc,limls):
    '''
    ========================================================================
    Description:
    Three machine learning models are created in which each model has its own best performing combinations. The prediction results of these 
    best performing combination are comapred with the target feature by means of table and plot.  
    ========================================================================
    '''
    # Initializing results comparison
    #------------------------------------------------------------------------
    overall_results = Result_comparison(models,loss,lossmlr,lossknn,optimizer,len_test)

    overall_results.read_data()                                             # Read data and store results
    overall_results.write_TargetVSpred()                                    # Write comparison Excel of prediciton results
    overall_results.write_Modelperformance()                                # Tabulate model performance results
    overall_results.plot_OverallResult(targetf,limacc,limls)                # Create overall result plots for analysis

if __name__ == '__main__':
   Overallresults()