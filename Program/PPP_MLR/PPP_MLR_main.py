'''
========================================================================
This program is a part of personal programming project
MLR program No: 1 
file: PPP_MLR_main
------------------------------------------------------------------------
MLR algorithm is implemented to predict the dependent feature from the independent features 
The program can train and test the model for a single combination of loss and SGD optimizer
To get a overall result of all the comibination execute PPP_MLR_createOverallresults.py
Adapted the program design of neural network in an attempt to create a single regression 
model that can perform both MLR and NN
========================================================================
'''

# Import created libraries
# -------------------------
from PPP_MLR import Data_preprocessing
from PPP_MLR import Single_layer,Updated_layer
from PPP_MLR import SGD_Optimizer
from PPP_MLR import MeanSquaredError_loss,RootMeanSquaredError_loss
from PPP_MLR import Accuracy

# Import required libraries
# -------------------------
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import os
import click
import timeit
#
# ======================================================================
# Construction of the model MLR Regression
# ======================================================================
#
class MLR_Regression:
    '''
    ========================================================================
    Description: 
    In this model class we train the model, plot training results, write trained parameters like weights and biases to a separate file, test the model, 
    plot testing results and finally perform prediction comparison between the predicted results and the original results.
    ========================================================================
    '''
    def __init__(self,data,no_of_epoch,loss_input,hp,dataset,predictor):
        '''
        ========================================================================
        Description:
        Initializing the inputs and created libraries, performing data preprocessing steps, Assigning single layer and 
        creating storage to store data.  
        ------------------------------------------------------------------------
        Parameters:
        data: Input dataset in .csv format with dependent feature as the last column 
        no_of_epoch: Training size; dtype -> int
        loss_input: Loss input; dtype -> str
        hp: Hyperparameters as dictionary
        dataset: Input to select dataset among trainingset and testset for prediction; dtype -> str
        predictor: Predictor input whether to execute the model as a pure predictor; dtype -> str
        ------------------------------------------------------------------------
        Note:
        -To check for overfitting select trainingset in dataset input
        ========================================================================
        '''
        self.no_of_epoch = no_of_epoch
        self.loss_input = loss_input 
        self.hp = hp 
        self.pred_dataset = dataset
        self.predictor = predictor

        # Data_preprocessing
         # ------------------------------------------------------------------------
        Preprocessed_data = Data_preprocessing()      # Initialization for data preprocessing
        X,y = Preprocessed_data.import_dataset(data)  # X: Independent feature, y: dependent feature                  
        self.scaled_X,self.scaled_y,self.mean_y,self.std_y = Preprocessed_data.feature_scaling(X,y) # Scaled features, mean and std for rescaling during comparison
        self.X_train, self.X_test,self.y_train,self.y_test = Preprocessed_data.split_dataset(self.scaled_X,self.scaled_y) # Splitting dataset into training and test set
        self.X_train = np.hstack((np.ones((len(self.X_train),1)), self.X_train)) # Updating the training set to have ones as first column for MLR
        self.X_test = np.hstack((np.ones((len(self.X_test),1)), self.X_test))    # Updating the testing set to have ones as first column MLR
       
        # Initialization for the single layer
        #------------------------------------------------------------------------
        self.single_layer =Single_layer(len(self.X_train[0]),self.hp['layers']['output_layer']) # Assigning layer from hp dictionary
        
        # Initialization for loss function and accuracy
        #------------------------------------------------------------------------
        self.loss_function_1 = MeanSquaredError_loss()
        self.loss_function_2 = RootMeanSquaredError_loss()
        self.model_accuracy = Accuracy()

        # Initialization for optimizers (Optimizer inputs are explained in PPP_MLR.py)
        #------------------------------------------------------------------------
        self.optimizer = SGD_Optimizer(self.hp['SGD']['learning_R'], self.hp['SGD']['learning_R_decay']) # Assigning optimizer inputs from hp dictionary

        # Creating Storage to store results during training
        #------------------------------------------------------------------------
        self.stored_lossValues = np.zeros(self.no_of_epoch)  # To store data loss
        self.stored_LearningR = np.zeros(self.no_of_epoch)   # To store learning rate update
        self.stored_Coeff_R2 = np.zeros(self.no_of_epoch)    # To store accuracy
        self.stored_trainingResult = []                      # To store predicted results after training
        self.stored_Parameters = []                          # To store parameters(weights and biases) after training

    def calculate_loss_Function(self,loss_Function,dataset):
        '''
        ========================================================================
        Description:
        To calculate data loss.
        ------------------------------------------------------------------------
        Parameters:
        loss_Function: Loss_function_1 or loss_function_2. loss calculation are performed based on the loss function
        dataset: Dependent feature(y_train or y_test); Array size -> [No of samples x No of neurons required in the output layer]
        ------------------------------------------------------------------------
        Return:
        loss: It is the data loss w.r.t predicted output and dependent feature; dtype -> float
        ========================================================================
        '''
        loss = loss_Function.calculate_loss(self.single_layer.output, dataset)
        return loss

    def calculate_back_prop(self,loss_Function,dataset):
        '''
        ========================================================================
        Description:
        To perform backward propagation in chain rule. 
        ------------------------------------------------------------------------
        Parameters:
        loss_Function: loss_function_1 or loss_function_2. Different backward propagation procedure are fallowed w.r.t loss function input
        dataset: Dependent feature(y_train); Array size -> [No of samples x No of neurons required in the output layer]
        ------------------------------------------------------------------------
        Note:
        -The backward propagation goes from the loss function to the single layer
        -Backward propagation of single layer creates derivatives of weights and biases which are used in optimizers
        -More details on back propagation and its inputs can be found in PPP_MLR.py
        ========================================================================
        '''
        loss_Function.back_prop(self.single_layer.output, dataset)  # Back prop of loss function(MSE or RMSE)
        self.single_layer.back_prop(loss_Function.inputs_derv)      # Back prop of single layer

    def calculate_optimizer(self,optimizer):
        '''
        ========================================================================
        Description:
        To perform optimization and update parameters in the single layer.
        ------------------------------------------------------------------------
        Parameters:
        optimizer: SGD optimizer. Parameters update varies based on the optimizer input
        ------------------------------------------------------------------------
        Return:
        LearningR: Current learning rate from the optimizer; dtype -> float      
        ------------------------------------------------------------------------
        Note:
        -Derivatives of weights and biases which are used in optimizers to update weights and biases in the single layer
        -More details on optimizer and its inputs can be found in PPP_MLR.py
        ========================================================================
        '''
        optimizer.learning_R_update()                     # Update learning rate in the optimizer
        LearningR= optimizer.C_learning_R                 # Return current learning rate from the optimizer
        optimizer.parameters_update(self.single_layer)    # Update the parameter of single layer 
        optimizer.itr_update()                            # Update iteration step in the optimizer
        return LearningR
    
    def begin_training(self):
        '''
        ========================================================================
        Description:
        Training the model in a loop from forward propagation, backward propagation to optimization w.r.t the no of epoch.
        The loss and accuracy value of training result should be good enough so that the model can use the update weights and biases to make predictions.
        ------------------------------------------------------------------------
        Conditions:
        Make sure tabulate package is installed or pip install tabulate  
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_Training and writes a file: trainingresults_lossinput.txt
        The file writes the complete history of training results such as accuracy, loss and learning rate updates for each epoch 
        ------------------------------------------------------------------------
        Note:
        -Parameters are updated in the single layer after training
        -Timetaken to perform forward prop, backward prop and optimizer is noted
        ========================================================================
        '''
        # Creating Storage to store time taken results during forward prop, back prop and optimization 
        #------------------------------------------------------------------------
        self.timetaken_toforw = []  # Time taken during forward prop
        self.timetaken_toback = []  # Time taken during back prop
        self.timetake_byopt = []    # Time taken during optimization

        # Select loss from the input param dictionary
        #------------------------------------------------------------------------
        input_param = {'loss':{'MSE':self.loss_function_1, 'RMSE':self.loss_function_2}}

        # Select loss function based on loss input
        #------------------------------------------------------------------------
        if self.loss_input in input_param['loss'].keys():
             loss_function =  input_param['loss'][self.loss_input]    

        for epoch in range(self.no_of_epoch):  # Training begins w.r.t the no of epoch
        
            # Forward Propagation (More details on forward propagation and its inputs can be found in PPP_MLR.py)
            #------------------------------------------------------------------------
            start_timeF = timeit.default_timer()                              # Start timer
            self.single_layer.forward_prop(self.X_train)                      # Forward prop of single layer with independent feature as an input
            self.timetaken_toforw.append(timeit.default_timer()-start_timeF)  # Stop timer. The time taken to perform forward prop is noted

            # Calculate Loss 
            #------------------------------------------------------------------------
            self.loss= self.calculate_loss_Function(loss_function,self.y_train)
            self.stored_lossValues[epoch] = self.loss                         # Store data loss

            # Calculate accuracy (More details on coefficient_of_determination and its inputs can be found in PPP_MLR.py)
            #------------------------------------------------------------------------
            self.Rsqr = self.model_accuracy.coefficient_of_determination(self.single_layer.output, self.y_train)
            self.stored_Coeff_R2[epoch] = self.Rsqr                            # Store accuracy
            
            # Back Propagation 
            #------------------------------------------------------------------------
            start_timeB = timeit.default_timer()                               # Start timer
            self.calculate_back_prop(loss_function,self.y_train)
            self.timetaken_toback.append(timeit.default_timer()-start_timeB)   # Stop timer. The time taken to perform back prop is noted

            # Perform Optimization
            #------------------------------------------------------------------------
            start_timeopt = timeit.default_timer()                             # Start timer
            LearningR = self.calculate_optimizer(self.optimizer)
            self.timetake_byopt.append(timeit.default_timer()-start_timeopt)   # Stop timer. The time taken to perform optimization is noted
            self.stored_LearningR[epoch]= LearningR                            # Store current learning rate

        # Forward prop for last optimizer update -> In the last epoch the optimizer gets updated and the loop exits 
        # Hence, generating result for forward prop with the updated parameters
        #-----------------------------------------------------------------------
        self.single_layer.forward_prop(self.X_train)

        # Calculate Loss for last optimizer update
        #------------------------------------------------------------------------
        self.loss= self.calculate_loss_Function(loss_function,self.y_train)
        self.stored_lossValues[epoch] = self.loss

        # Calculate accuracy for last optimizer update
        #------------------------------------------------------------------------
        self.Rsqr = self.model_accuracy.coefficient_of_determination(self.single_layer.output, self.y_train)
        self.stored_Coeff_R2[epoch] = self.Rsqr
        self.stored_LearningR[epoch] = self.optimizer.C_learning_R # Updating the current learning rate

        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Results_Training'):
            os.makedirs('Results_Training')
        save_path = os.path.abspath('Results_Training') # Save the file to the created directory

        # Creating a dataframe for the training results so that the results can be written in a tablulated form
        #------------------------------------------------------------------------
        writedata = pd.DataFrame({'Epoch': np.arange(0,self.no_of_epoch,1),'R^2':self.stored_Coeff_R2,'Loss': self.stored_lossValues, 'lR': self.stored_LearningR})

        # writing training results for every 100th epoch into the file -> trainingresults_lossinput.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path,'trainingresults_%s.txt'%(self.loss_input)) , 'w') as f:
            
            print('Training results',file=f)                                                # Creating title infos before the training begins
            print('\nLoss and optimizer used: '+str(self.loss_input)+', SGD \n', file=f)    # Details on loss and optimizer of the model
            print(writedata.iloc[::100,:].to_markdown(tablefmt='grid',index=False),file=f)  # Tabulating the results in grid format without index
            print('\nAbbreviations:',file=f)                                                # Adding required abbreviations
            print('R^2: Coefficient of determination(Accuracy)\nlR: learning rate',file=f)
        
        # store training results
        #------------------------------------------------------------------------
        self.stored_trainingResult.append(self.single_layer.output)

    def write_trainedparameters(self,writeparam_as):
        '''
        ========================================================================
        Description:
        To write the updated weights and biases after training which later used when the program runs as pure predictor.
        ------------------------------------------------------------------------
        Parameters:
        writeparam_as: selecting format to write from npy or txt; dtype -> str
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_Parameters and writes a file: parametersMLR_lossinput.npy / .txt
        The file writes updated weights and biases after training  
        ------------------------------------------------------------------------
        Note:
        -The file should be written in .npy format to read back in during prediciton
        -The format .txt is only for reading purpose
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Results_Parameters'):
            os.makedirs('Results_Parameters')
        save_path = os.path.abspath('Results_Parameters') # Save the file to the created directory

        # Store parameters
        #------------------------------------------------------------------------
        weights,biases = self.single_layer.read_weights_biases()
        self.stored_Parameters.append(weights),self.stored_Parameters.append(biases) # Store updated weights and biases from single layer

        # writing updated parameters into the file -> parametersMLR_lossinput.npy / .txt
        #------------------------------------------------------------------------
        if writeparam_as == 'txt':
            with open(os.path.join(save_path,'parametersMLR_%s.txt'% (self.loss_input)), 'w') as f:

                print('Parameters: Weights and biases (after training)',file=f)            # Creating title infos before writing
                print('\nLoss and optimizer used: '+str(self.loss_input)+', SGD', file=f)  # Details on loss and optimizer of the model

                # Creating writing structure for reading in .txt format
                print('\nSingle layer: Weights\n',file=f)
                print(self.stored_Parameters[0],file=f)
                print('\nSingle layer: Biases\n',file=f)
                print(self.stored_Parameters[1],file=f)
        else:
            with open(os.path.join(save_path,'parametersMLR_%s.npy'% (self.loss_input)), 'wb') as f:

                for idx in range(len(self.stored_Parameters)):
                    np.save(f,self.stored_Parameters[idx]) # Saving the parameters in .npy format
    
    def read_parameters(self):
        '''
        ========================================================================
        Description:
        If the model is running as a pure predictor the model reads in the writen updated weights and biases from the 
        file: parametersMLR_lossinput.npy in the directory: Results_Parameters.
        ------------------------------------------------------------------------
        Conditions:
        The dataset used when the model runs as a predictor should be similar to or an extension of the datas that are used for training the model
        ------------------------------------------------------------------------
        Return:
        parameters: return the read in updated weights and biases of single layer after training as a list
        ------------------------------------------------------------------------
        Note / Example :
        -When the model runs as a predictor i.e. when predictor is ON the model doesnot perform training
        ========================================================================
        ''' 
        open_path = os.path.abspath('Results_Parameters')                            # Specify the directory to open the file
        filename = os.path.join(open_path,'parametersMLR_%s.npy'%(self.loss_input))  # Accessing the files w.r.t the combinations(loss) 
        with open(filename, 'rb') as f:                                              # Read in .npy format file
            weight,bias = np.load(f), np.load(f)                                     # Loading in the updated parameters of single layer
        parameters = [weight,bias]                                                   # returning the parameters in form of a list 
        return parameters

    def begin_prediction(self):
        '''
        ========================================================================
        Description:
        The updated parameters from training the model is now used to predict dependent feature from unseen independent features. 
        The predicted results are then tested against know dependent feature.
        ------------------------------------------------------------------------
        Conditions:
        The model should be executed as a predictor only after training the model with dataset similar to that of the predictor dataset
        The loss used during predictor should be same as the loss used while training the model
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_Training or Results_MLR_predictor depending on predictor input and writes a file: testingresults_lossinput.txt
        The file writes the testing results such as accuracy and loss of the model
        ------------------------------------------------------------------------
        Note:
        -The idea behind the predictor is to avoid re-training of the model if prediction has to be done on new datasets that are similar to or an extension 
        of the datasets that was used during the model training  
        -So when the model runs as a predictor no training will be done and will consider the input dataset directly for prediction
        ========================================================================
        '''
        # Select loss from the input param dictionary
        #-----------------------------------------------------------------------
        input_param = {'MSE':self.loss_function_1, 'RMSE':self.loss_function_2}

        # Select loss function based on loss input
        #------------------------------------------------------------------------
        if self.loss_input in input_param.keys():
            loss_function =  input_param[self.loss_input]    
       
        if self.predictor == 'OFF':                      # Program runs normally when the predictor is OFF
            # Select dataset to perform prediction
            #------------------------------------------------------------------------
            if self.pred_dataset == 'testset':           # To test the performance of the trained parameters
                self.pred_input = self.X_test            # For testset X_test (independent features) will be the prediction input
                self.test_metric = self.y_test           # And y_test (dependent features) will be the testing metric or original output
            elif self.pred_dataset == 'trainingset':     # To test whether the model overfits(model correctness: The model should overfit)
                self.pred_input = self.X_train           # In that case the trainined dataset X_train (independent features) will be the prediction input
                self.test_metric = self.y_train          # And y_train (dependent features) will be the testing metric or original output
        
        elif self.predictor == 'ON':                                      # When the predictor is ON we use the read in parameters to perform prediction 
            parameters = self.read_parameters()
            self.single_layer =Updated_layer(parameters[0],parameters[1]) # Recreating single layer by using the parameters of single layer in updated layer
            self.pred_input = np.hstack((np.ones((len(self.scaled_X.values),1)), self.scaled_X.values)) # Updating the scaled features with ones as first column for MLR and using it as predictor input
            self.test_metric = self.scaled_y.values                       # Considering scaled dependent feature of the predictor dataset as testing metric or original output

        # Forward Propagation (More details on forward propagation and its inputs can be found in PPP_MLR.py)
        #------------------------------------------------------------------------
        self.single_layer.forward_prop(self.pred_input)                   # Forward prop of single layer with independent feature as an input

        # Calculate Loss 
        #------------------------------------------------------------------------
        self.loss_pred = self.calculate_loss_Function(loss_function,self.test_metric) # The loss will be calculated again with the test metric

        # Calculate accuracy (More details on coefficient_of_determination and its inputs can be found in PPP_MLR.py)
        #------------------------------------------------------------------------
        self.Rsqr_pred = self.model_accuracy.coefficient_of_determination(self.single_layer.output, self.test_metric) # The acc will be calculated again with the test metric
        
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        path = 'Results_MLR_predictor' if self.predictor == 'ON' else 'Results_Testing'
        if not os.path.exists(path):
                os.makedirs(path)
        save_path = os.path.abspath(path) # Save the file to the created directory

        # writing testing results into the file -> testingresults_lossinput.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path,'testingresults_%s.txt'% (self.loss_input)) , 'w') as f:

            print('Testing results',file=f)                                                      # Creating title infos
            print('\nLoss and optimizer used: '+str(self.loss_input)+', SGD \n', file=f)         # Details on loss and optimizer of the model
            print('testing:  ' + f'R^2: {self.Rsqr_pred}, ' + f'loss: {self.loss_pred}',file=f)  # Writing prediction accuracy and loss
            print('\nAbbreviations:',file=f)                                                     # Adding required abbreviations
            print('R^2: Coefficient of determination(Accuracy)',file=f) 
    
    def prediction_comparison(self):
        '''
        ========================================================================
        Description:
        To write result comparison between the predicted and the target result in a well tabulated manner.
        ------------------------------------------------------------------------
        Conditions:
        Make sure tabulate package is installed or pip install tabulate
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Results_TargetVSpred or Results_MLR_predictor depending on predictor input and writes a file: resultcomparison_lossinput.txt 
        The file writes the testing results and the test metric of the model
        ========================================================================
        '''
        # Rescaling computed prediction and target values
        #------------------------------------------------------------------------
        rescaled_y_test = (self.test_metric*self.std_y + self.mean_y).reshape(-1,) # Using the mean and std from scaling to rescale the target result as per dataset
        rescaled_y_pred = (self.single_layer.output*self.std_y + self.mean_y).reshape(-1,) # Using the mean and std from scaling to rescale the predicted result as per dataset
        
        # To check the correctness of rescaling, accuracy is computed for the rescaled results and then compared with the original accuracy result
        #------------------------------------------------------------------------
        rescaled_Rsqr = self.model_accuracy.coefficient_of_determination(rescaled_y_pred, rescaled_y_test)
        
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        path = 'Results_MLR_predictor' if self.predictor == 'ON' else 'Results_TargetVSpred' # If runs as a pure predictor save comparison results to -> Results_MLR_predictor
        if not os.path.exists(path):
            os.makedirs(path)        
        save_path = os.path.abspath(path) # Save the file to the created directory

        # Creating a dataframe for comparison results so that the results can be written in a tablulated form
        #------------------------------------------------------------------------
        writedata = pd.DataFrame({'target':rescaled_y_test,'predicted':rescaled_y_pred})

        # writing comparison of the results and the accuracy into the file -> resultcomparision_lossinput.txt
        #------------------------------------------------------------------------
        with open(os.path.join(save_path, 'resultcomparison_%s.txt'% (self.loss_input)), 'w') as f:

            print('Result comparison: Target vs Predicted',file=f)                          # Creating title infos before writing
            print('\nLoss and optimizer used: '+str(self.loss_input)+', SGD \n', file=f)    # Details on loss and optimizer of the model
            print(writedata.to_markdown(tablefmt='grid',floatfmt='.0f',index=False),file=f) # Tabulating the results in grid format without index

            print('\nComparing R^2 for correctness: ' + f'prediction: {self.Rsqr_pred}, vs ' + f're-scaled prediction: {rescaled_Rsqr}',file=f) # checking for rescaling correctness
            print('\nAbbreviations:',file=f)                                                # Adding required abbreviations
            print('R^2: Coefficient of determination(Accuracy)',file=f)
            if self.predictor == 'OFF':                                                     # Program runs normally when the predictor is OFF
                print('\nTime taken in seconds:\nForward propagation = %f; Backward propagation = %f; Optimization = %f'%(np.mean(self.timetaken_toforw),
                np.mean(self.timetaken_toback),np.mean(self.timetake_byopt)), file=f)       # Writing time taken results of forward prop, back prop and optimizer

    def write_time(self,timetaken_permodel,timetaken_totrain,timetaken_topred):
        '''
        ========================================================================
        Description:
        Writing time taken results for training, prediction and entire model to execute for each combinations
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exist: Results_TargetVSpred or Results_ANN_predictor depending on predictor input 
        and append time taken results to the file: resultcomparision_lossinput.txt
        ------------------------------------------------------------------------
        Note:
        If the predictor is ON the time taken for the model to train is ignored 
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        path = 'Results_MLR_predictor' if self.predictor == 'ON' else 'Results_TargetVSpred' # If runs as a pure predictor save comparison results to -> Results_MLR_predictor
        if not os.path.exists(path):
            os.makedirs(path)
        save_path = os.path.abspath(path) # Save the file to the created directory

        # Write timing results for each combination -> Model, training and prediction
        #------------------------------------------------------------------------
        with open(os.path.join(save_path,'resultcomparison_%s.txt' % (self.loss_input)) , 'a') as f:
            
            if self.predictor == 'OFF':
                print('Model = %f; Training = %f; Prediction = %f' %(timetaken_permodel,timetaken_totrain,timetaken_topred), file=f)
            else:
                print('\nTime taken in seconds:\nModel = %f; Prediction = %f' %(timetaken_permodel,timetaken_topred), file=f)

    def plot_trainingresults(self):
        '''
        ========================================================================
        Description:
        Creating a plot for training results which includes training ouput,target result for training, loss, accuracy and learning rate update.
        Each plot depends on the loss input.
        ------------------------------------------------------------------------
        Output:
        Creates a directory: Plots_MLR_Main and saves a plot: plot_trainingresults_lossinput.png
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_MLR_Main'):
                os.makedirs('Plots_MLR_Main')
        save_path = os.path.abspath('Plots_MLR_Main') # Save the file to the created directory
        
        # Plot for Training set results
        #------------------------------------------------------------------------
        plt.suptitle('Training result (@'+str(self.loss_input)+ ' & SGD)',fontsize = 24)          # Creating suptitle for the plot
        plt.subplots_adjust(top=0.90)                                                             # Adjusting the title position
        ax1 = plt.subplot2grid((2,3),(0,0), colspan = 3)                                          # Plotting in grids -> occupy all the three columns of the first row
        ax1.set_title('Target vs Trained')                                                        # Set plot title
        ax1.set_xlabel('No of samples')                                                           # Set label for x-axis -> will be the no of sample
        ax1.set_ylabel('Sample values')                                                           # Set label for y-axis -> will be the values of each sample
        ax1.plot(self.y_train*self.std_y + self.mean_y, 'o--',markersize = 7,color = 'red', label = 'Target data') # plotting training and target results
        ax1.plot(self.stored_trainingResult[0]*self.std_y + self.mean_y,'o-',markersize = 4,color = 'blue',label='Trained data ($R^2$ = %.3f; loss = %.4f)'%(self.Rsqr,self.loss))
        ax1.legend()
    
        # Plot for training loss
        #------------------------------------------------------------------------
        ax2 = plt.subplot2grid((2,3),(1,0))                                                       # Plotting in grids -> last row first column
        ax2.set_title('Loss')                                                                     # Set plot title
        ax2.set_xlabel('No of epoch')                                                             # Set label for x-axis -> will be the no of epoch
        ax2.set_ylabel('Loss value')                                                              # Set label for y-axis -> will be loss values of training
        self.ex_label = 'loss = %.4f'%(self.loss)                                                 # Creating label w.r.t loss input
        ax2.plot(np.arange(0,self.no_of_epoch,1),self.stored_lossValues, label = self.ex_label)   
        ax2.legend()
        
        # Plot for Coefficient of determination during training
        #------------------------------------------------------------------------
        ax3 = plt.subplot2grid((2,3),(1,1))                                                       # Plotting in grids -> last row second column
        ax3.set_title('Coefficient of determination')                                             # Set plot title
        ax3.set_xlabel('No of epoch')                                                             # Set label for x-axis -> will be the no of epoch
        ax3.set_ylabel('$R^2$ value')                                                             # Set label for y-axis -> will be accuracy values of training
        ax3.plot(np.arange(0,self.no_of_epoch,1),self.stored_Coeff_R2,label = '$R^2$ = %.3f'% self.Rsqr)
        ax3.legend()

        # Create label for learning rate w.r.t optimizer input
        #------------------------------------------------------------------------
        self.ex_label = 'lr = '+str(self.hp['SGD']['learning_R'])+'; lr_d = '+str(self.hp['SGD']['learning_R_decay'])

        # Plot for Learning rate update during training
        #------------------------------------------------------------------------    
        ax4 = plt.subplot2grid((2,3),(1,2))                                                       # Plotting in grids -> last row last column
        ax4.set_title('Learning rate')                                                            # Set plot title
        ax4.set_xlabel('No of epoch')                                                             # Set label for x-axis -> will be the no of epoch
        ax4.set_ylabel('Learning rate value')                                                     # Set label for y-axis -> will be learning rate values of training
        ax4.plot(np.arange(0,self.no_of_epoch,1),self.stored_LearningR,label = self.ex_label)
        ax4.legend()
        fig = plt.gcf()                                                                           # Get the current figure                                                                  
        fig.set_size_inches((22.5 ,11.5),forward = False)                                         # Assigning plot size (width,height)                    
        fig.set_dpi(300)                                                                          # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path,'plot_trainingresults_%s.png'%(self.loss_input)),bbox_inches='tight')
        plt.clf()                                                                                 # Cleaning the current figure after saving

    def plot_testingresults(self):
        '''
        ========================================================================
        Description:
        Creating a plot for testing results which includes predicted and the target result.
        Each plot depends on the loss input.
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exists: Plots_MLR_Main and returns a plot: plot_testingresults_lossinput.png     
        ------------------------------------------------------------------------
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_MLR_Main'):
                os.makedirs('Plots_MLR_Main')
        save_path = os.path.abspath('Plots_MLR_Main') # Save the file to the created directory
        
        # Plot for Testing set results
        #------------------------------------------------------------------------
        plt.title('Testing result: Target vs Prediction(@'+str(self.loss_input)+ ' & SGD)')  # Set plot title
        plt.xlabel('No of samples')                                                          # Set label for x-axis -> will be the no of sample
        plt.ylabel('Sample values')                                                          # Set label for y-axis -> will be the values of each sample
        plt.plot(self.test_metric*self.std_y + self.mean_y, 'o--',markersize = 7,color = 'red', label = 'Target data') # Plotting testing and target results
        plt.plot(self.single_layer.output*self.std_y + self.mean_y,'o-',markersize = 4,color = 'blue', label = 'Predicted data ($R^2$ = %.3f; loss = %.4f)'%(self.Rsqr_pred,self.loss_pred))
        plt.legend()
        fig = plt.gcf()                                                                      # Get the current figure
        fig.set_size_inches((9.5 ,7),forward = False)                                        # Assigning plot size (width,height)
        fig.set_dpi(300)                                                                     # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path,'plot_testingresults_%s.png'%(self.loss_input)),bbox_inches='tight') # Saving the plot
        plt.clf()                                                                            # Cleaning the current figure after saving

    def plot_testingerror(self):
        '''
        ========================================================================
        Description:
        Creating a error plot for the error between the target and the predicted results.
        Each plot depends on the loss input.
        ------------------------------------------------------------------------
        Output:
        Creates a directory if one does not exists: Plots_MLR_Main and returns a plot: plot_testingerror_lossinput.png     
        ------------------------------------------------------------------------
        ========================================================================
        '''
        # Creating a directory if one does not exists
        #------------------------------------------------------------------------
        if not os.path.exists('Plots_MLR_Main'):
                os.makedirs('Plots_MLR_Main')
        save_path = os.path.abspath('Plots_MLR_Main') # Save the file to the created directory
        
        # Plot for Testing set error
        #------------------------------------------------------------------------
        plt.title('Testing set error(@'+str(self.loss_input)+ ' & SGD)')                     # Set plot title
        plt.xlabel('No of samples')                                                          # Set label for x-axis -> will be the no of sample
        plt.ylabel('Error value')                                                            # Set label for y-axis -> error value
        Error = (self.test_metric*self.std_y + self.mean_y)-(self.single_layer.output*self.std_y + self.mean_y) # Compute error
        plt.plot(Error, 'o--',markersize = 7,color = 'red', label = 'Error')                 # Plotting testing set error
        plt.axhline(y=0, color = 'blue', linestyle = '--')                                   # Make horizontal line at zero error
        plt.legend()
        fig = plt.gcf()                                                                      # Get the current figure
        fig.set_size_inches((9.5 ,7),forward = False)                                        # Assigning plot size (width,height)
        fig.set_dpi(300)                                                                     # Set resolution in dots per inch
        fig.savefig(os.path.join(save_path,'plot_testingerror_%s.png'%(self.loss_input)),bbox_inches='tight') # Saving the plot
        plt.clf()                                                                            # Cleaning the current figure after saving

def check_inputs(no_of_epoch,pred_dataset,loss,makeplot,writeparam_as,predictor): 
    '''
    ========================================================================
    Description:
    To check whether certain user inputs are within the required limits.
    ------------------------------------------------------------------------
    Parameters:
    no_of_epoch: Training size; dtype -> int
    pred_dataset: Input to select dataset among trainingset and testset for prediction; dtype -> str
    loss: Loss input; dtype -> str 
    makeplot: Input whether to make plots or not; dtype -> int
    writeparam_as: Writing format input to write updated parameters; dtype -> str 
    predictor: Predictor input whether to execute the model as a pure predictor; dtype -> str
    ------------------------------------------------------------------------
    Note:
    -If the inputs are not within the options range, program exits by throwing an error with possible inputs
    ========================================================================
    '''
    # Checking whether the input is correct or wrong
    #-----------------------------------------------------------------------
    inputset1 = [pred_dataset,loss,makeplot,writeparam_as,predictor] # Grouping similar inputs and their options togather
    optionset1 = [['testset','trainingset','pred_dataset'],['MSE','RMSE','loss'],[0,1,'makeplot'],['npy','txt','writeparam_as'],['ON','OFF','predictor']]

    for idx,input in enumerate(inputset1): # Checking for correctness
        if (not input == optionset1[idx][0]) and (not input == optionset1[idx][1]): # If the inputs are not within the options range program exits by throwing an error mentioning possible inputs
            sys.exit('Error: Recheck '+str(optionset1[idx][2])+' input\nPossible inputs: '+str(optionset1[idx][0]) +' or '+str(optionset1[idx][1]))

    if(no_of_epoch <= 0):                                 # Checking epoch input, it should be greater than zero
        sys.exit('Error: no_of_epoch input must be > 0')  # Program exits if the input is lesser than or equal to zero
#
# ======================================================================
# User selection of Adjustable inputs -> Implementing click
# ======================================================================
#
@click.command()
@click.option('--data',nargs=1,type=str,default='fatigue_dataset.csv',help='Enter input dataset.csv: last column must be the target feature')
@click.option('--no_of_epoch',nargs=1,type=int,default=10001,help='Enter No of epoch for training(>0)')
@click.option('--pred_dataset',nargs=1,type=str,default='testset',help='Select dataset for prediction: [testset or trainingset]')
@click.option('--loss',nargs=1,type=str,default='MSE',help='Select loss: [MSE or RMSE]')
@click.option('--sgd',nargs=2,type=float,default=([0.13,1e-1]),help='Enter SGD_optimizer input')
@click.option('--makeplot',nargs=1,type=int,default=1,help='Select 0 or 1 to makeplot')
@click.option('--writeparam_as',nargs=1,type=str,default='npy',help='Select write format: npy or txt')
@click.option('--predictor',nargs=1,type=str,default='OFF',help='Select ON or OFF for only prediction')
#
# ============================================================================================================================================
#                                                      CREATING MODEL --> MLR REGRESSION
# ============================================================================================================================================
#
def MLR_regression(data,no_of_epoch,pred_dataset,loss,sgd,makeplot,writeparam_as,predictor):
    '''
    ========================================================================
    Description:
    This MLR REGRESSION model can execute two different combinations w.r.t loss @ SGD optimizer with one combination at a time.
    ========================================================================
    '''
    # Check certain user inputs
    #------------------------------------------------------------------------
    check_inputs(no_of_epoch,pred_dataset,loss,makeplot,writeparam_as,predictor)

    # Adjustable hyperparameters must be tunned separately w.r.t the dataset used as it can impact the accuracy of the model
    # Creating the input dictionary of hp
    #------------------------------------------------------------------------    
    SGD_lr,SGD_lrd = sgd       # SGD hyperparameter inputs in form of tuple  @RMSE: SGD_lr = 0.5 so that the does not get stuck in local minima

    hyp_paramMLR = {'layers':{'output_layer':1},'SGD':{'learning_R':SGD_lr, 'learning_R_decay':SGD_lrd}}

    # Initializing the model
    # Note: 
    # If predictor is ON all results of prediction w.r.t loss and optimizer inputs are present in the directory -> Results_MLR_predictor
    # The files present in the directory -> testingresults_lossinput_optimizerinput.txt and resultcomparision_lossinput_optimizerinput.txt
    #-----------------------------------------------------------------------
    start_time1 = timeit.default_timer()                                    # Start timer to check the time taken by the model to execute
    model = MLR_Regression(data,no_of_epoch,loss,hyp_paramMLR,pred_dataset,predictor)

    # Train the model
    #------------------------------------------------------------------------
    if predictor == 'OFF':                                                  # If prediction is ON the model will not be trained
        start_time2 = timeit.default_timer()
        model.begin_training()
        timetaken_totrain = timeit.default_timer()-start_time2              # Stop time. The time taken to train the model is noted; dtype -> float
 
        # Plot training results
        #------------------------------------------------------------------------
        if makeplot == 1:                                                   # If makeplot is satisfied(1) make the plots
            model.plot_trainingresults()
        
        # write trained parameters
        model.write_trainedparameters(writeparam_as)                        # Writing trained parameters
    else:
        timetaken_totrain = 0                                               # When predictor is ON training will not happen thus time taken to train wil be zero

    # Test the model
    #------------------------------------------------------------------------
    start_time3 = timeit.default_timer()                                    # Start timer to check the time taken for prediction in the model
    model.begin_prediction()
    timetaken_topred = timeit.default_timer()-start_time3                   # Stop time. The time taken to prediction in the model is noted; dtype -> float

    # Plot prediction results and error
    #------------------------------------------------------------------------
    if (predictor == 'OFF') and (makeplot == 1):                            # If predictor is OFF and makeplot is satisfied make the plots
        model.plot_testingresults()
        model.plot_testingerror()

    # Write comparison data of the attained results with the original results
    #------------------------------------------------------------------------
    model.prediction_comparison()

    timetaken_permodel = timeit.default_timer()-start_time1                 # Stop time. The time taken by the model to execute is noted; dtype -> float

    # Write timing results for each combination -> Model, training and prediction
    #------------------------------------------------------------------------
    model.write_time(timetaken_permodel,timetaken_totrain,timetaken_topred)

if __name__ == '__main__':
   MLR_regression()