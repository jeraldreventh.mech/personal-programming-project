'''
========================================================================
This program is a part of personal programming project
MLR program No: 2 
file: PPP_MLR_createOverallresults
------------------------------------------------------------------------
Creates results for two combinations of loss and SGD optimizer with Predictor ON and OFF in a single attempt
The created results are then used in file: PPP_MLR_combination.py to make plots with all the combinations for analysis
Condition: If the results are created with predictor: ON then only predictor results are created which is not sufficient to execute PPP_MLR_combination.py
======================================================================== 
'''
# Import required libraries
# -------------------------
import pandas as pd
import subprocess
import sys
import os
import click
import timeit
#
# ======================================================================
# Creating results for all the combinations of MLR Regression model 
# ======================================================================
#
class Create_Overallresults:
    '''
    ========================================================================
    Description: 
    Creating overall results for two combinations of loss and SGD optimizer in MLR regression.
    ========================================================================
    '''
    
    def createRes_toplot(self,data,no_of_epoch,pred_dataset,loss,sgd,sgd_rmse,makeplot,writeparam_as,pred):
        '''
        ========================================================================
        Description:
        To create a master code that runs PPP_MLR_main.py using subprocess for combinations and for any different hyperparameters.
        This helps us to study the impact of tunning or changing any parameter on the combinations.
        ------------------------------------------------------------------------
        Parameters:
        All parameter details including datatype and number of arguments required can be found in click options implemented below
        Execute the program PPP_MLR_createOverallresults.py --help to get more details
        ------------------------------------------------------------------------
        Return:
        timetaken_percode: Time taken per combination code to execute in seconds; dtype -> float    
        ========================================================================
        '''
        timetaken_percode = [] # Store time taken per combination code to execute

        # Using all the inputs to execute PPP_MLR_main.py w.r.t loss
        #------------------------------------------------------------------------
        for ls in loss: 
            sgd = sgd_rmse if ls == 'RMSE' else sgd # Selecting different hyperparameters for the optimizer in case of RMSE loss

            start_time = timeit.default_timer()     # Start timer
            subprocess.call([sys.executable,'.\PPP_MLR_main.py', '--data=' '%s' % data, '--no_of_epoch=' '%d' % no_of_epoch, '--pred_dataset=' '%s' % pred_dataset, \
            '--loss=' '%s' % ls, '--sgd=' '%s' % str(sgd[0]), '%s' % str(sgd[1]), '--makeplot=' '%d' % makeplot, '--writeparam_as=' '%s' % writeparam_as, \
            '--predictor=' '%s' % pred])
            timetaken_percode.append(timeit.default_timer()-start_time) # Stop timer. The taken taken by each combination code to execute is noted

        return timetaken_percode

def write_time(percode,overall):
    '''
    ========================================================================
    Description:
    To write results of time taken by the two combinations to execute and time taken by each combination to execute.
    ------------------------------------------------------------------------
    Parameters:
    percode: time taken for each combination in seconds; dtype -> float
    overall: time taken for all combinations in seconds; dtype -> float
    ------------------------------------------------------------------------
    Conditions:
    Make sure tabulate package is installed or pip install tabulate
    ------------------------------------------------------------------------
    Output:
    writes a file: timetaken_overall.txt to the directory: Results_TargetVSpred     
    ========================================================================
    '''
    # Creating a dataframe for time taken results so that the results can be written in a tablulated form
    #------------------------------------------------------------------------ 
    comb = ['MSE_SGD','RMSE_SGD'] # Combinations list to tabulate
    writedata = pd.DataFrame({'Combinations':comb,'Time(s)':percode})

    # writing time taken results into the file -> timetaken_overall.txt
    #------------------------------------------------------------------------
    save_path = os.path.abspath('Results_TargetVSpred') # Save the file to the created directory
    with open(os.path.join(save_path,'timetaken_overall.txt') , 'w') as f:
        
        print('Time taken in create overall results',file=f)                               # Creating title infos before writing
        print('\nTime taken by each code to execute:\n', file=f)                           # Writing results of time taken for each combination
        print(writedata.to_markdown(tablefmt='grid',floatfmt='.6f',index=False),file=f)    # make sure tabulate package is installed or pip install tabulate
        print('\nTime taken by two combination to execute:',overall,file=f)                # Writing results of time taken for two combinations

def check_inputs(no_of_epoch,pred_dataset,makeplot,writeparam_as,predictor): 
    '''
    ========================================================================
    Description:
    To check whether certain user inputs are within the required limits.
    ------------------------------------------------------------------------
    Parameters:
    no_of_epoch: Training size; dtype -> int
    pred_dataset: Input to select dataset among trainingset and testset for prediction; dtype -> str
    makeplot: Input whether to make plots or not; dtype -> int
    writeparam_as: Writing format input to write updated parameters; dtype -> str 
    predictor: Predictor input whether to execute the model as a pure predictor; dtype -> str
    ------------------------------------------------------------------------
    Note:
    -If the inputs are not within the options range, program exits by throwing an error with possible inputs
    ========================================================================
    '''
    # Checking whether the input is correct or wrong
    #-----------------------------------------------------------------------
    inputset = [pred_dataset,makeplot,writeparam_as,predictor] # Grouping similar inputs and their options togather
    optionset = [['testset','trainingset','pred_dataset'],[0,1,'makeplot'],['npy','txt','writeparam_as'],['ON','OFF','predictor']]

    for idx,input in enumerate(inputset):
        if (not input == optionset[idx][0]) and (not input == optionset[idx][1]): # If the inputs are not within the options range program exits by throwing an error mentioning possible inputs
            sys.exit('Error: Recheck '+str(optionset[idx][2])+' input\nPossible inputs: '+str(optionset[idx][0]) +' or '+str(optionset[idx][1]))

    if(no_of_epoch <= 0):                                 # Checking epoch input, it should be greater than zero
        sys.exit('Error: no_of_epoch input must be > 0')  # Program exits if the input is lesser than or equal to zero
#
# ======================================================================
# User selection of Adjustable inputs -> Implementing click
# ======================================================================
#
@click.command()
@click.option('--data',nargs=1,type=str,default='fatigue_dataset.csv',help='Enter input dataset.csv: last column must be the target feature')
@click.option('--no_of_epoch',nargs=1,type=int,default=10001,help='Enter No of epoch for training(>0)')
@click.option('--pred_dataset',nargs=1,type=str,default='testset',help='Select dataset for prediction: [testset or trainingset]')
@click.option('--losslist',nargs=2,type=str,default=['MSE','RMSE'],help='Loss input as a list')
@click.option('--sgd',nargs=2,type=float,default=([0.13,1e-1]),help='Enter SGD_optimizer input')
@click.option('--sgd_rmse',nargs=2,type=float,default=([0.5,1e-1]),help='Enter SGD_optimizer input @RMSE')
@click.option('--makeplot',nargs=1,type=int,default=0,help='Select 0 or 1 to makeplot')
@click.option('--writeparam_as',nargs=1,type=str,default='npy',help='select write format: npy or txt')
@click.option('--predictor',nargs=1,type=str,default='OFF',help='Select ON or OFF for only prediction')
#
# ============================================================================================================================================
#                                                      CREATING OVERALL RESULTS --> MLR REGRESSION
# ============================================================================================================================================
#
def MLR_createOverallresults(data,no_of_epoch,pred_dataset,losslist,sgd,sgd_rmse,makeplot,writeparam_as,predictor):
    '''
    ========================================================================
    Description:
    This CREATE OVERALL RESULTS make prediction results for two combination of losses and tunes hyperparameters to find best predicting model.
    ========================================================================
    '''
    # Check certain user inputs
    #-----------------------------------------------------------------------
    check_inputs(no_of_epoch,pred_dataset,makeplot,writeparam_as,predictor)

    # Initializing overall results
    #------------------------------------------------------------------------
    createfiles = Create_Overallresults()

    # Create Overall results for the combination to make plot and later analyse
    # Note: 
    # If predictor is ON all results of prediction for different combinations are present in the directory -> Results_MLR_predictor
    # The files present in the directory -> testingresults_lossinput.txt and resultcomparision_lossinput.txt
    #------------------------------------------------------------------------
    start_time = timeit.default_timer()                                      # Start timer to check the time taken for overall results
    timetaken_percode = createfiles.createRes_toplot(data,no_of_epoch,pred_dataset,losslist,sgd,sgd_rmse,makeplot,writeparam_as,predictor)
    timetaken_overall = timeit.default_timer()-start_time                    # Stop time. The time taken to execute overall results is noted; dtype -> float

    # Write time taken result
    #------------------------------------------------------------------------
    write_time(timetaken_percode,timetaken_overall)

if __name__ == '__main__':
   MLR_createOverallresults()