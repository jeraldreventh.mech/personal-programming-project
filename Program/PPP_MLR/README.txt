DIRECTORY DETAILS
-----------------
Executable Files: 3
- PPP_MLR_main.py
- PPP_MLR_createOverallresults.py
- PPP_MLR_combination.py

Input files: 2
- PPP_MLR.py
- fatigue_dataset.csv(default dataset)/ fatigue_Selecteddataset.csv/ fatigue_Selecteddataset_7F.csv / fatigue_Selecteddataset_3F.csv

The fallowing commands will create all the results present in this directory
- python .\PPP_MLR_createOverallresults.py --makeplot 1
- python .\PPP_MLR_combination.py
- python .\PPP_MLR_createOverallresults.py --predictor ON

The results generated with other dataset can be found in the directory 'Results_OtherDatasets'
Create results by executing 
- python .\PPP_MLR_createOverallresults.py --makeplot 1 --data dataset.csv (dataset.csv: fatigue_Selecteddataset.csv)
- python .\PPP_MLR_combination.py
Refer Manual for how to generate results for different cases
----------------------------------------------------------------------------------------------------

# Details regarding the files and directories created can be referred from "Table 8" in the report.

File name nomenclature for the files created
Example: plot_trainingresults_MSE --> filename_x

x --> Loss

x    | Loss     
MSE  | Mean Squared Error
RMSE | Root Mean Squared Error

Analysis:
--------
Result analysis is performed on the created .xlsx file present in the directory Results_TargetVSpred and saved
as resultcomparisonMLR_Analysed.xlsx. The formulas present in this file shall be maintained. The only change
required is adjusting the row entry in the formulas with respect to the number of samples(n) in the test set.

For example:
The results of column D can be created by copying and pasting the formula present in it for each row with respect to the size of the test set. 
For the formulas present in row 100 and 101, column D, the row entry is D3:Dn. Here, n is the number of samples in the test set + 2.