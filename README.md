Python program implementing Machine learning algorithms from scratch to predict fatigue strength of steel from composition and processing parameters.

AIM:
The main goal of this project is to develop data assessment tools and prediction models from scratch to get an intuitive feeling for the dataset and 
to predict mechanical properties of steel (Fatigue strength).